# Cesta de Compras

### Live Diagrams

#### Class diagram

![Class diagram for the application](/services/diagram/test-cesta-de-compras/package/cesta_de_compras.uml?showClassifierCompartments=Always&showStaticFeatures=true&showClasses=true&showAssociationEndName=true&showAttributes=true&showOperations=true&showComments=true&showParameters=true&showAssociationEndMultiplicity=true&showMinimumVisibility=Public&showFeatureVisibility=false&showParameterNames=false&showDerivedElements=false)

#### Statechart diagram

![Statechart diagram for the application](/services/diagram/test-cesta-de-compras/package/cesta_de_compras.uml?showStateMachines=true)

